if (Drupal.jsEnabled) {
  var ndx = location.href.lastIndexOf('/')+1;
  var nid = location.href.slice(ndx);

  var chess112_ndx = location.href.indexOf('/chess112/game_in_progress');
  var before_chess112 = location.href.slice(0, chess112_ndx);
  var http_ndx = before_chess112.indexOf('//') + 2;
  var after_http = before_chess112.slice(http_ndx);
  var base_ndx = after_http.indexOf('/');
  var base = after_http.slice(base_ndx);

  var timeout = 1000;
  var count = 0;

  var prev_color = 0;

  $.ajax({
    cache: false,
    url: base + '/chess112/color_to_move',
    dataType: 'text',
    data: ({
      nid: nid
    }),
    success: function(color_to_move) { prev_color = color_to_move; },
    error: function(xmlhttp) { alert('ERROR: ' + xmlhttp.status + ' ' + xmlhttp.responseText); }
  });
}

if (Drupal.jsEnabled) {
  $(function() {
//alert("ready");
    var board_flipped = $('#board_flipped').html();
    var machine_state = 1;
    var from_column = 0;
    var from_row = 0;
    var to_column = 0;
    var to_row = 0;
    $('span').filter('.chess112_square').each(function(i) {

      $(this).hover(
        function () {
          board_flipped = $('#board_flipped').html();
          if (board_flipped != ' ') {
            id = 111 - this.id;
          } else {
            id = this.id;
          }
          $('#square').html(' - '+id);
        },
        function () {
          $('#square').html(' ');
        }
      );

      $(this).click(function() {
        var letters = "abcdefghijklmn";
        board_flipped = $('#board_flipped').html();
        if (board_flipped != ' ') {
          var col = 13 - this.id % 14;
          var row = parseInt(this.id / 14) + 1;
        } else {
          var col = this.id % 14;
          var row = 8 - parseInt(this.id / 14);
        }
        switch (machine_state) {
          case 1:
            $('#from_column').html(letters[col]);
            from_column = letters[col];
            $('#from_row').html(row);
            from_row = row;
            $('#to_column').html('.');
            $('#to_row').html('.');
            machine_state++;
            $('#machine_state').html(machine_state);
            $('span').filter('.chess112_square').each(function(i) {
              $(this).removeClass('selected');
            });
            $(this).addClass('selected');
            break;
          case 2:
            $('#to_column').html(letters[col]);
            to_column = letters[col];
            $('#to_row').html(row);
            to_row = row;
            machine_state--;
            $('#machine_state').html(machine_state);
            $(this).addClass('selected');
            break;
        }
      });
    });
    $('#submit_move').click(function() {
      var process_move = function(s) {
/*
        $('span').filter('.chess112_square').each(function(i) {
          $(this).removeClass('selected');
        });
/*
        a = s.split(' ');
        $('span').filter('.chess112_square').each(function(i) {
          var f = false;
          for (var x = 1; x < a.length; x++) {
            board_flipped = $('#board_flipped').html();
            if (board_flipped != ' ') {
              var ndx = 111 - a[x];
            } else {
              var ndx = a[x];
            }
            if (this.id == ndx) {
              f = true;
              break;
            }
          }
          if (f) {
            $(this).addClass('selected');
          }
        });
*/
        if (s.substring(0, 5) == 'ERROR') {
          alert(s);
          $('span').filter('.chess112_square').each(function(i) {
            $(this).removeClass('selected');
          });
          $('#working').html(' ');
        }
      }

      if (from_column == 0) {
        alert('No piece selected');
      }
      else if (to_column == 0) {
        alert('No destination selected');
      }
      else {
        $('#working').html(' - working...');
        $.ajax({
          cache: false,
          url: base + '/chess112/make_move',
          dataType: 'text',
          data: ({
            nid: nid,
            from_column: from_column,
            from_row: from_row,
            to_column: to_column,
            to_row: to_row
          }),
          success: process_move,
          error: function(xmlhttp) { alert('ERROR: ' + xmlhttp.status + ' ' + xmlhttp.responseText); }
        });
        return false;
      }

    });

    setTimeout ('checkBoardState()', timeout);
  });
}

function checkBoardState() {
  $.get(base + '/chess112/color_to_move', { nid: nid },
    function(color_to_move) {
      if (color_to_move != prev_color) {
        $.get(base + '/chess112/board_state', { nid: nid },
          function(board_state) {
            $('span').filter('.chess112_square').each(function(i) {
              board_flipped = $('#board_flipped').html();
              if (board_flipped != ' ') {
                id = 111 - this.id;
              }
              else {
                id = this.id;
              }
              var ndx = id * 3;
              var new_piece = board_state.substring(ndx, ndx + 2);
              var src = $('#'+this.id+'.chess112_square img').attr('src');
              var chess = src.indexOf('Chess_') + 6;
              var old_piece = src.substring(chess, chess+2);
              src = src.replace('Chess_'+old_piece, 'Chess_'+new_piece);
              $('#'+this.id+'.chess112_square img').attr('src', src);
            });
          }
        );
        $('span').filter('.chess112_square').each(function(i) {
          $(this).removeClass('selected');
        });
        if (color_to_move == 0) {
          $('#color_to_move').html('<b>White</b> to move');
        }
        else {
          $('#color_to_move').html('<b>Black</b> to move');
        }
        prev_color = color_to_move;
        $('#working').html(' ');
      }
    }
  );
  count += 1;
  $('#count').html(count);
  setTimeout ('checkBoardState()', timeout);
}

function letter_to_number(letter) {
  switch (letter) {
    case 'a': number = 0; break;
    case 'b': number = 1; break;
    case 'c': number = 2; break;
    case 'd': number = 3; break;
    case 'e': number = 4; break;
    case 'f': number = 5; break;
    case 'g': number = 6; break;
    case 'h': number = 7; break;
    case 'i': number = 8; break;
    case 'j': number = 9; break;
    case 'k': number = 0; break;
    case 'l': number = 11; break;
    case 'm': number = 12; break;
    case 'n': number = 13; break;
  }
  return number;
}

