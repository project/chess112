<?php

/**
 * @file
 * chess112.moves.inc: legal chess moves for chess112
 *
 * NOTE: This module was inspired by (and borrows from) the chess module created by Eion Bailey ( http://drupal.org/project/chess );
 */

/**
 * chess112_mailbox112().
 */
function chess112_mailbox112() {
  return array(
     33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,
     49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,
     65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,
     81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,
     97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
    113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126,
    129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142,
    145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158
  );
}

/**
 * chess112_mailbox192().
 */
function chess112_mailbox192() {
  return array(
    -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
    -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
    -1,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  -1,
    -1,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  -1,
    -1,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  -1,
    -1,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  -1,
    -1,  56,  57,  58,  59,  60,  61,  62,  63,  64,  65,  66,  67,  68,  69,  -1,
    -1,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  -1,
    -1,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,  96,  97,  -1,
    -1,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,  -1,
    -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
    -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1
  );
}

/**
 * chess112_piece_at().
 */
function chess112_piece_at($board_state, $square) {
  $piece = drupal_substr($board_state, $square * 3, 2);
  return $piece;
}

/**
 * chess112_color_at().
 */
function chess112_color_at($board_state, $square) {
  $color = drupal_substr($board_state, $square * 3 + 1, 1);
//drupal_set_message('color at: '. $color);
  return $color;
}

/**
 * chess112_rook_moves().
 */
function chess112_rook_moves($mb192, $mb112, $board_state, $from_square, $to_square, $p, $o) {
  $moves = array();
  for ($i = 1; $mb192[$mb112[$from_square] + $i] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square + $i) == $p) break;
    $moves[] = $from_square + $i;
    if (chess112_color_at($board_state, $from_square + $i) == $o) break;
  }
  for ($i = 1; $mb192[$mb112[$from_square] - $i] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square - $i) == $p) break;
    $moves[] = $from_square - $i;
    if (chess112_color_at($board_state, $from_square - $i) == $o) break;
  }
  for ($i = 1; $mb192[$mb112[$from_square] + $i * 16] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square + $i * 14) == $p) break;
    $moves[] = $from_square + $i * 14;
    if (chess112_color_at($board_state, $from_square + $i * 14) == $o) break;
  }
  for ($i = 1; $mb192[$mb112[$from_square] - $i * 16] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square - $i * 14) == $p) break;
    $moves[] = $from_square - $i * 14;
    if (chess112_color_at($board_state, $from_square - $i * 14) == $o) break;
  }
  return $moves;
}

/**
 * chess112_knight_moves().
 */
function chess112_knight_moves($mb192, $mb112, $board_state, $from_square, $to_square, $p) {
  $moves = array();
  if ($mb192[$mb112[$from_square] - 33] > -1 && chess112_color_at($board_state, $from_square - 29) != $p) $moves[] = $from_square - 29;
  if ($mb192[$mb112[$from_square] - 31] > -1 && chess112_color_at($board_state, $from_square - 27) != $p) $moves[] = $from_square - 27;
  if ($mb192[$mb112[$from_square] - 18] > -1 && chess112_color_at($board_state, $from_square - 16) != $p) $moves[] = $from_square - 16;
  if ($mb192[$mb112[$from_square] - 14] > -1 && chess112_color_at($board_state, $from_square - 12) != $p) $moves[] = $from_square - 12;
  if ($mb192[$mb112[$from_square] + 33] > -1 && chess112_color_at($board_state, $from_square + 29) != $p) $moves[] = $from_square + 29;
  if ($mb192[$mb112[$from_square] + 31] > -1 && chess112_color_at($board_state, $from_square + 27) != $p) $moves[] = $from_square + 27;
  if ($mb192[$mb112[$from_square] + 18] > -1 && chess112_color_at($board_state, $from_square + 16) != $p) $moves[] = $from_square + 16;
  if ($mb192[$mb112[$from_square] + 14] > -1 && chess112_color_at($board_state, $from_square + 12) != $p) $moves[] = $from_square + 12;
  return $moves;
}

/**
 * chess112_bishop_moves().
 */
function chess112_bishop_moves($mb192, $mb112, $board_state, $from_square, $to_square, $p, $o) {
  $moves = array();
  for ($i = 1; $mb192[$mb112[$from_square] + $i * 17] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square + $i * 15) == $p) break;
    $moves[] = $from_square + $i * 15;
    if (chess112_color_at($board_state, $from_square + $i * 15) == $o) break;
  }
  for ($i = 1; $mb192[$mb112[$from_square] - $i * 17] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square - $i * 15) == $p) break;
    $moves[] = $from_square - $i * 15;
    if (chess112_color_at($board_state, $from_square - $i * 15) == $o) break;
  }
  for ($i = 1; $mb192[$mb112[$from_square] + $i * 15] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square + $i * 13) == $p) break;
    $moves[] = $from_square + $i * 13;
    if (chess112_color_at($board_state, $from_square + $i * 13) == $o) break;
  }
  for ($i = 1; $mb192[$mb112[$from_square] - $i * 15] > -1; $i++) {
    if (chess112_color_at($board_state, $from_square - $i * 13) == $p) break;
    $moves[] = $from_square - $i * 13;
    if (chess112_color_at($board_state, $from_square - $i * 13) == $o) break;
  }
  return $moves;
}

/**
 * chess112_queen_moves().
 */
function chess112_queen_moves($mb192, $mb112, $board_state, $from_square, $to_square, $p, $o) {
  $moves = array();
  if ($p == 'l') {
    $rmoves = chess112_rook_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l', 'd');
    $bmoves = chess112_bishop_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l', 'd');
  }
  else {
    $rmoves = chess112_rook_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd', 'l');
    $bmoves = chess112_bishop_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd', 'l');
  }
  foreach ($rmoves as $rmove) {
    $moves[] = $rmove;
  }
  foreach ($bmoves as $bmove) {
    $moves[] = $bmove;
  }
  return $moves;
}

/**
 * chess112_king_moves().
 */
function chess112_king_moves($mb192, $mb112, $board_state, $from_square, $to_square, $p) {
  $moves = array();
  if ($mb192[$mb112[$from_square] + 1] > -1 && chess112_color_at($board_state, $from_square + 1) != $p) $moves[] = $from_square + 1;
  if ($mb192[$mb112[$from_square] - 1] > -1 && chess112_color_at($board_state, $from_square - 1) != $p) $moves[] = $from_square - 1;
  if ($mb192[$mb112[$from_square] + 16] > -1 && chess112_color_at($board_state, $from_square + 14) != $p) $moves[] = $from_square + 14;
  if ($mb192[$mb112[$from_square] - 16] > -1 && chess112_color_at($board_state, $from_square - 14) != $p) $moves[] = $from_square - 14;
  if ($mb192[$mb112[$from_square] + 17] > -1 && chess112_color_at($board_state, $from_square + 15) != $p) $moves[] = $from_square + 15;
  if ($mb192[$mb112[$from_square] - 17] > -1 && chess112_color_at($board_state, $from_square - 15) != $p) $moves[] = $from_square - 15;
  if ($mb192[$mb112[$from_square] + 15] > -1 && chess112_color_at($board_state, $from_square + 13) != $p) $moves[] = $from_square + 13;
  if ($mb192[$mb112[$from_square] - 15] > -1 && chess112_color_at($board_state, $from_square - 13) != $p) $moves[] = $from_square - 13;
  return $moves;
}

/**
 * chess112_white_pawn_moves().
 */
function chess112_white_pawn_moves($mb192, $mb112, $board_state, $from_square, $to_square, $o) {
  $moves = array();
  if ($from_square >= 84 && $from_square < 98 && chess112_piece_at($board_state, $from_square - 14) == 'zz' && chess112_piece_at($board_state, $from_square - 28) == 'zz') {
    $moves[] = $from_square - 28;
  }
  if ($mb192[$mb112[$from_square] - 16] > -1 && chess112_piece_at($board_state, $from_square - 14) == 'zz') {
    $moves[] = $from_square - 14;
  }
  if ($mb192[$mb112[$from_square] - 17] > -1 && chess112_color_at($board_state, $from_square - 15) == $o) $moves[] = $from_square - 15;
  if ($mb192[$mb112[$from_square] - 15] > -1 && chess112_color_at($board_state, $from_square - 13) == $o) $moves[] = $from_square - 13;
  return $moves;
}

/**
 * chess112_black_pawn_moves().
 */
function chess112_black_pawn_moves($mb192, $mb112, $board_state, $from_square, $to_square, $o) {
  $moves = array();
  if ($from_square >= 14 && $from_square < 28 && chess112_piece_at($board_state, $from_square + 14) == 'zz' && chess112_piece_at($board_state, $from_square + 28) == 'zz') {
    $moves[] = $from_square + 28;
  }
  if ($mb192[$mb112[$from_square] + 16] > -1 && chess112_piece_at($board_state, $from_square + 14) == 'zz') {
    $moves[] = $from_square + 14;
  }
  if ($mb192[$mb112[$from_square] + 17] > -1 && chess112_color_at($board_state, $from_square + 15) == $o) $moves[] = $from_square + 15;
  if ($mb192[$mb112[$from_square] + 15] > -1 && chess112_color_at($board_state, $from_square + 13) == $o) $moves[] = $from_square + 13;
  return $moves;
}

/**
 * chess112_legal_moves().
 */
function chess112_legal_moves($board_state, $from_square, $from_piece, $from_color, $to_square, $to_piece, $to_color) {
  $mb192 = chess112_mailbox192();
  $mb112 = chess112_mailbox112();
  $moves = array();
  $piece = $from_piece . $from_color;
  switch ($piece) {
    case 'rl': return chess112_rook_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l', 'd');
    case 'rd': return chess112_rook_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd', 'l');
    case 'nl': return chess112_knight_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l');
    case 'nd': return chess112_knight_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd');
    case 'bl': return chess112_bishop_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l', 'd');
    case 'bd': return chess112_bishop_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd', 'l');
    case 'ql': return chess112_queen_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l', 'd');
    case 'qd': return chess112_queen_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd', 'l');
    case 'kl': return chess112_king_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l');
    case 'kd': return chess112_king_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd');
    case 'pl': return chess112_white_pawn_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'd');
    case 'pd': return chess112_black_pawn_moves($mb192, $mb112, $board_state, $from_square, $to_square, 'l');
    default: return $moves;
  }
}

