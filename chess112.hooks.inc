<?php

/**
 * @file
 * chess112.hooks.inc: Drupal hooks for chess112
 *
 * NOTE: This module was inspired by (and borrows from) the chess module created by Eion Bailey ( http://drupal.org/project/chess );
 */

/**
 * Implementation of hook_access().
 */
function chess112_access($op, $node, $account) {
  if ($op == 'create') {
    return user_access('create chess112', $account);
  }
  if ($op == 'update') {
    if (user_access('edit own chess112', $account) && ($account->uid == $node->uid)) {
      return TRUE;
    }
  }
  if ($op == 'delete') {
    if (user_access('delete own chess112', $account) && ($account->uid == $node->uid)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Implementation of hook_block().
 */
function chess112_block($op = 'list', $delta = 0) {
  $block = array();
  $items = array();
  switch ($op) {
    case 'list':
      $block[0]['info'] = t('Chess 112');
      $block[1]['info'] = t('Move list');
      break;
    case 'view':
      switch ($delta) {
        case 0:
          $block['subject'] = t('Chess 112');
          $items[] = l(t('Create a game'), 'node/add/chess112');
          $items[] = l(t('Join a game'), 'chess112/join_a_game');
          $items[] = l(t('Watch a game'), 'chess112/watch_a_game');
          $items[] = l(t('My games'), 'chess112/my_games');
          $items[] = l(t('Tutorial'), 'chess112/tutorial');
          $items[] = l(t('Credits'), 'chess112/credits');
          $block['content'] = theme('item_list', $items);
          break;
        case 1:
          if (strstr($_SERVER['REDIRECT_URL'], 'game_in_progress')) {
            $block['subject'] = t('Move list');
            $header = array(t('No.'), t('Player'), array('data' => t('Move'), 'colspan' => 5));
            $rows = array();
            $result = db_query("SELECT * FROM {chess112_move} WHERE nid = %d ORDER BY timestamp DESC LIMIT 0, 5", $_SESSION['nid']);
            while ($data = db_fetch_object($result)) {
              ($data->move_number % 2 > 0) ? $player = t('white') : $player = t('black');
              if ($data->captured_piece == 'z') {
                $rows[] = array($data->move_number, $player, $data->piece, $data->from_column . $data->from_row, $data->to_column . $data->to_row, '-');
              }
              else {
                $rows[] = array($data->move_number, $player, $data->piece, $data->from_column . $data->from_row, $data->to_column . $data->to_row, $data->captured_piece);
              }
            }
            $block['content'] = theme('table', $header, $rows);
          }
          break;
      }
      break;
  }
  return $block;
}

/**
 * Implementation of hook_delete().
 */
function chess112_delete($node) {
  db_query('DELETE FROM {chess112_game} WHERE nid = %d', $node->nid);
  db_query('DELETE FROM {chess112_move} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_form().
 */
function chess112_form(&$node, $form_state) {
  $type = node_get_types('type', $node);
  $result = db_query("SELECT uid, mail FROM {users} WHERE uid > 0 AND status = 1");
  $users = array();
  $users[0] = 'wait for challenger';
  while ($user = db_fetch_array($result)) {
    $users[$user['uid']] = $user['mail'];
  }
  $form['white'] = array(
    '#type' => 'select',
    '#title' => t('White player'),
    '#required' => TRUE,
    '#options' => $users,
    '#default_value' => $node->user_white,
    '#weight' => -4,
  );
  $form['black'] = array(
    '#type' => 'select',
    '#title' => t('Black player'),
    '#required' => TRUE,
    '#options' => $users,
    '#default_value' => $node->user_black,
    '#weight' => -3,
  );
  $form['#validate'][] = 'chess112_form_validate';
  return $form;
}

/**
 * Implementation of hook_form_validate().
 */
function chess112_form_validate($form, &$form_state) {
  global $user;
  if (($form_state['values']['white'] != $user->uid) && ($form_state['values']['black'] != $user->uid)) {
    form_set_error('white', 'At least one player must be the originator.');
  }
  $count = db_result(db_query("SELECT count(*) as count FROM {chess112_game} WHERE user_white = %d AND user_black = %d", $form_state['values']['white'], $form_state['values']['black']));
  if ($count > 0) {
    form_set_error('white', 'Only one game at a time is allowed between two given players.');
  }
}

/**
 * Implementation of hook_init().
 */
function chess112_init() {
  if (isset($_SESSION['nid'])) {
    db_query("UPDATE {blocks} SET region = 'left' WHERE module = 'chess112' AND delta = '1'");
  }
  else {
    db_query("UPDATE {blocks} SET region = 'none' WHERE module = 'chess112' AND delta = '1'");
    $_SESSION['nid'] = 0;
  }
  if (!isset($_SESSION['board_flipped'])) {
    $_SESSION['board_flipped'] = FALSE;
  }
}

/**
 * Implementation of hook_insert().
 */
function chess112_insert($node) {
  $chess112_board_state  = 'rd nd bd rd nd bd qd kd bd nd rd bd nd rd ';
  $chess112_board_state .= 'pd pd pd pd pd pd pd pd pd pd pd pd pd pd ';
  $chess112_board_state .= 'zz zz zz zz zz zz zz zz zz zz zz zz zz zz ';
  $chess112_board_state .= 'zz zz zz zz zz zz zz zz zz zz zz zz zz zz ';
  $chess112_board_state .= 'zz zz zz zz zz zz zz zz zz zz zz zz zz zz ';
  $chess112_board_state .= 'zz zz zz zz zz zz zz zz zz zz zz zz zz zz ';
  $chess112_board_state .= 'pl pl pl pl pl pl pl pl pl pl pl pl pl pl ';
  $chess112_board_state .= 'rl nl bl rl nl bl ql kl bl nl rl bl nl rl ';
  $current_player = WHITE;
  $check = 0;
  $checkmate = 0;
  db_query("INSERT INTO {chess112_game} (nid, user_white, user_black, board_state, next_move, check_, checkmate) VALUES (%d, %d, %d, '%s', '%s', %d, %d)", $node->nid, $node->white, $node->black, $chess112_board_state, $current_player, $check, $checkmate);
}

/**
 * Implementation of hook_load().
 */
function chess112_load($node) {
  return db_fetch_object(db_query('SELECT user_white, user_black FROM {chess112_game} WHERE nid = %d', $node->nid));
}

/**
 * Implementation of hook_menu().
 */
function chess112_menu() {
  $items = array();
  $items['chess112/join_a_game'] = array(
    'title' => 'Games waiting for a challenger',
    'page callback' => 'chess112_join_a_game',
    'access callback' => 'user_access',
    'access arguments' => array('create chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/challenge/%/%'] = array(
    'page callback' => 'chess112_challenge',
    'page arguments' => array(2, 3),
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/watch_a_game'] = array(
    'title' => 'Games being played',
    'page callback' => 'chess112_watch_a_game',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/my_games'] = array(
    'title' => 'My games',
    'page callback' => 'chess112_my_games',
    'access callback' => 'user_access',
    'access arguments' => array('create chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/delete_game'] = array(
    'title' => 'Delete game',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('chess112_delete_game_form'),
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/game_in_progress/%'] = array(
    'title' => 'Game in progress',
    'page callback' => 'chess112_game_in_progress',
    'page arguments' => array(2),
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/make_move'] = array(
    'title' => 'Make move',
    'page callback' => 'chess112_make_move',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/color_to_move'] = array(
    'title' => 'Color to move',
    'page callback' => 'chess112_color_to_move',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/the_move'] = array(
    'title' => 'The move',
    'page callback' => 'chess112_the_move',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/board_state'] = array(
    'title' => 'Board state',
    'page callback' => 'chess112_board_state',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/flip_board/%'] = array(
    'title' => 'Flip board',
    'page callback' => 'chess112_flip_board',
    'page arguments' => array(2),
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/credits'] = array(
    'title' => 'Credits',
    'page callback' => 'chess112_credits',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  $items['chess112/tutorial'] = array(
    'title' => 'Tutorial',
    'page callback' => 'chess112_tutorial',
    'access callback' => 'user_access',
    'access arguments' => array('access chess112'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function chess112_node_info() {
  return array(
    'chess112' => array(
      'name' => t('Chess112'),
      'module' => 'chess112',
      'description' => t('Chess 112 is a chess variant with an 8 by 14 square board.'),
      'has_title' => FALSE,
      'has_body' => FALSE,
      'locked' => TRUE,
    )
  );
}

/**
 * Implementation of hook_perm().
 */
function chess112_perm() {
  return array('access chess112', 'create chess112', 'edit own chess112', 'delete own chess112');
}

/**
 * Implementation of hook_update().
 */
function chess112_update($node) {
  if ($node->revision) {
    chess112_insert($node);
  }
  else {
    db_query("UPDATE {chess112_game} SET user_white = %d, user_black = %d WHERE nid = %d", $node->white, $node->black, $node->nid);
  }
}

