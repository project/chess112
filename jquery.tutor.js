if (Drupal.jsEnabled) {
  $(function() {
    $('a#tutorial').click(function() {
      tutor = window.open('','','left=20,top=20,width=500,height=500,toolbar=0,resizable=0,scrollbars=1');
      tutor.document.write('<b>Practice</b><ul>');
      tutor.document.write('<li>Click "Chess 112" > "Create a game".</li>');
      tutor.document.write('<li>Select yourself as both "White player" and "Black player".</li>');
      tutor.document.write('<li>Click the "Save" button.</li>');
      tutor.document.write('<li>Click "Chess 112" > "My games".</li>');
      tutor.document.write('<li>Click "go to game" for the game you just created.</li>');
      tutor.document.write('<li>Optionally, click "Flip board" for the black player view.</li>');
      tutor.document.write('<li>Click a white piece or pawn, then a square, then "Submit".</li>');
      tutor.document.write('<li>Click a black piece or pawn, then a square, then "Submit".</li>');
      tutor.document.write('<li>Illegal moves will be flagged and rejected.</li>');
      tutor.document.write('</ul>');
      tutor.document.write('<b>Create a game, and wait for a challenger</b><ul>');
      tutor.document.write('<li>Click "Chess 112" > "Create a game".</li>');
      tutor.document.write('<li>Select yourself as either "White player" or "Black player".</li>');
      tutor.document.write('<li>Leave "wait for challenger" as the remaining option.</li>');
      tutor.document.write('<li>Click the "Save" button.</li>');
      tutor.document.write('<li>Click "Chess 112" > "My games".</li>');
      tutor.document.write('<li>Click "go to game" for the game you just created.</li>');
      tutor.document.write('<li>If you are playing white, make your first move.</li>');
      tutor.document.write('<li>Wait for a response or an email saying you have a challenger.</li>');
      tutor.document.write('</ul>');
      tutor.document.write('<b>Challenge</b><ul>');
      tutor.document.write('<li>Click "Chess 112" > "Join a game".</li>');
      tutor.document.write('<li>Click "play white" or "play black" for the game you want to join.</li>');
      tutor.document.write('<li>Click "go to game" for the game you just joined.</li>');
      tutor.document.write('<li>If you are playing white, make your first move.</li>');
      tutor.document.write('<li>Click your opponent\'s handle (user id) to send him an email.</li>');
      tutor.document.write('<li>Wait for him to respond with a move or an email.</li>');
      tutor.document.write('</ul>');
      tutor.document.write('<b>Watch a game</b><ul>');
      tutor.document.write('<li>Click "Chess 112" > "Watch a game".</li>');
      tutor.document.write('<li>Click "go to game" for the game you want to watch.</li>');
      tutor.document.write('</ul>');
    });
  });
}
