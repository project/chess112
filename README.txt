$Id

Directory structure
-------------------

Create the "sites/all/modules" folder if you don't already have one.
Place the "chess112" folder and its contents under that folder.

Installation
------------

Go to "Administer" > "Modules" and enable "Chess112"
Go to "Administer" > "Blocks" and place "Chess 112" into the "Left sidebar".

Permission settings
-------------------

Go to "Administer" > "Permissions" > "chess112 module".
Check all 4 permission settings for authenticated user.
Go to "Administer" > "Permissions" > "node module".
Check the "administer nodes" permission setting for authenticated user.
This is just to eliminate an annoying message that pops up if you don't check it.

Create, join, watch a game
--------------------------

Click "Chess 112" > "Tutorial".
