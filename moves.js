function init_pieces() {
  pieces = new Array(
   'br', 'bn', 'bb', 'br', 'bn', 'bb', 'bq', 'bk', 'bb', 'bn', 'br', 'bb', 'bn', 'br',
   'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp',
   'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp',
   'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp',
   'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp',
   'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp', 'sp',
   'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp',
   'wr', 'wn', 'wb', 'wr', 'wn', 'wb', 'wq', 'wk', 'wb', 'wn', 'wr', 'wb', 'wn', 'wr'
  );
  return pieces;
}

function mailbox112() {
  return new Array(
     33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46,
     49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
     65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78,
     81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94,
     97, 98, 99,100,101,102,103,104,105,106,107,108,109,110,
    113,114,115,116,117,118,119,120,121,122,123,124,125,126,
    129,130,131,132,133,134,135,136,137,138,139,140,141,142,
    145,146,147,148,149,150,151,152,153,154,155,156,157,158
  );
}

function mailbox192() {
  return new Array(
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, -1,
    -1, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, -1,
    -1, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, -1,
    -1, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, -1,
    -1, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, -1,
    -1, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, -1,
    -1, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, -1,
    -1, 98, 99,100,101,102,103,104,105,106,107,108,109,110,111, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
  );
}

function white_rook_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == BLACK_PICK_UP || state == BLACK_PUT_DOWN) return moves;
  for(i = 1; mb192[mb112[id]+i] > -1; i++) {
    if(pieces[id+i][0] == 'w') break;
    moves.push(id+i);
    if(pieces[id+i][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i] > -1; i++) {
    if(pieces[id-i][0] == 'w') break;
    moves.push(id-i);
    if(pieces[id-i][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]+i*16] > -1; i++) {
    if(pieces[id+i*14][0] == 'w') break;
    moves.push(id+i*14);
    if(pieces[id+i*14][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i*16] > -1; i++) {
    if(pieces[id-i*14][0] == 'w') break;
    moves.push(id-i*14);
    if(pieces[id-i*14][0] == 'b') break;
  }
  return moves;
}

function black_rook_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == WHITE_PICK_UP || state == WHITE_PUT_DOWN) return moves;
  for(i = 1; mb192[mb112[id]+i] > -1; i++) {
    if(pieces[id+i][0] == 'b') break;
    moves.push(id+i);
    if(pieces[id+i][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i] > -1; i++) {
    if(pieces[id-i][0] == 'b') break;
    moves.push(id-i);
    if(pieces[id-i][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]+i*16] > -1; i++) {
    if(pieces[id+i*14][0] == 'b') break;
    moves.push(id+i*14);
    if(pieces[id+i*14][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i*16] > -1; i++) {
    if(pieces[id-i*14][0] == 'b') break;
    moves.push(id-i*14);
    if(pieces[id-i*14][0] == 'w') break;
  }
  return moves;
}

function white_knight_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == BLACK_PICK_UP || state == BLACK_PUT_DOWN) return moves;
  if(mb192[mb112[id]-33] > -1 && pieces[id-29][0] != 'w') moves.push(id-29);
  if(mb192[mb112[id]-31] > -1 && pieces[id-27][0] != 'w') moves.push(id-27);
  if(mb192[mb112[id]-18] > -1 && pieces[id-16][0] != 'w') moves.push(id-16);
  if(mb192[mb112[id]-14] > -1 && pieces[id-12][0] != 'w') moves.push(id-12);
  if(mb192[mb112[id]+33] > -1 && pieces[id+29][0] != 'w') moves.push(id+29);
  if(mb192[mb112[id]+31] > -1 && pieces[id+27][0] != 'w') moves.push(id+27);
  if(mb192[mb112[id]+18] > -1 && pieces[id+16][0] != 'w') moves.push(id+16);
  if(mb192[mb112[id]+14] > -1 && pieces[id+12][0] != 'w') moves.push(id+12);
  return moves;
}

function black_knight_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == WHITE_PICK_UP || state == WHITE_PUT_DOWN) return moves;
  if(mb192[mb112[id]-33] > -1 && pieces[id-29][0] != 'b') moves.push(id-29);
  if(mb192[mb112[id]-31] > -1 && pieces[id-27][0] != 'b') moves.push(id-27);
  if(mb192[mb112[id]-18] > -1 && pieces[id-16][0] != 'b') moves.push(id-16);
  if(mb192[mb112[id]-14] > -1 && pieces[id-12][0] != 'b') moves.push(id-12);
  if(mb192[mb112[id]+33] > -1 && pieces[id+29][0] != 'b') moves.push(id+29);
  if(mb192[mb112[id]+31] > -1 && pieces[id+27][0] != 'b') moves.push(id+27);
  if(mb192[mb112[id]+18] > -1 && pieces[id+16][0] != 'b') moves.push(id+16);
  if(mb192[mb112[id]+14] > -1 && pieces[id+12][0] != 'b') moves.push(id+12);
  return moves;
}

function white_bishop_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == BLACK_PICK_UP || state == BLACK_PUT_DOWN) return moves;
  for(i = 1; mb192[mb112[id]+i*17] > -1; i++) {
    if(pieces[id+i*15][0] == 'w') break;
    moves.push(id+i*15);
    if(pieces[id+i*15][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]+i*15] > -1; i++) {
    if(pieces[id+i*13][0] == 'w') break;
    moves.push(id+i*13);
    if(pieces[id+i*13][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i*17] > -1; i++) {
    if(pieces[id-i*15][0] == 'w') break;
    moves.push(id-i*15);
    if(pieces[id-i*15][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i*15] > -1; i++) {
    if(pieces[id-i*13][0] == 'w') break;
    moves.push(id-i*13);
    if(pieces[id-i*13][0] == 'b') break;
  }
  return moves;
}

function black_bishop_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == WHITE_PICK_UP || state == WHITE_PUT_DOWN) return moves;
  for(i = 1; mb192[mb112[id]+i*17] > -1; i++) {
    if(pieces[id+i*15][0] == 'b') break;
    moves.push(id+i*15);
    if(pieces[id+i*15][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]+i*15] > -1; i++) {
    if(pieces[id+i*13][0] == 'b') break;
    moves.push(id+i*13);
    if(pieces[id+i*13][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i*17] > -1; i++) {
    if(pieces[id-i*15][0] == 'b') break;
    moves.push(id-i*15);
    if(pieces[id-i*15][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i*15] > -1; i++) {
    if(pieces[id-i*13][0] == 'b') break;
    moves.push(id-i*13);
    if(pieces[id-i*13][0] == 'w') break;
  }
  return moves;
}

function white_queen_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == BLACK_PICK_UP || state == BLACK_PUT_DOWN) return moves;
  for(i = 1; mb192[mb112[id]+i] > -1; i++) {
    if(pieces[id+i][0] == 'w') break;
    moves.push(id+i);
    if(pieces[id+i][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i] > -1; i++) {
    if(pieces[id-i][0] == 'w') break;
    moves.push(id-i);
    if(pieces[id-i][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]+i*16] > -1; i++) {
    if(pieces[id+i*14][0] == 'w') break;
    moves.push(id+i*14);
    if(pieces[id+i*14][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i*16] > -1; i++) {
    if(pieces[id-i*14][0] == 'w') break;
    moves.push(id-i*14);
    if(pieces[id-i*14][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]+i*17] > -1; i++) {
    if(pieces[id+i*15][0] == 'w') break;
    moves.push(id+i*15);
    if(pieces[id+i*15][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]+i*15] > -1; i++) {
    if(pieces[id+i*13][0] == 'w') break;
    moves.push(id+i*13);
    if(pieces[id+i*13][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i*17] > -1; i++) {
    if(pieces[id-i*15][0] == 'w') break;
    moves.push(id-i*15);
    if(pieces[id-i*15][0] == 'b') break;
  }
  for(i = 1; mb192[mb112[id]-i*15] > -1; i++) {
    if(pieces[id-i*13][0] == 'w') break;
    moves.push(id-i*13);
    if(pieces[id-i*13][0] == 'b') break;
  }
  return moves;
}

function black_queen_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == WHITE_PICK_UP || state == WHITE_PUT_DOWN) return moves;
  for(i = 1; mb192[mb112[id]+i] > -1; i++) {
    if(pieces[id+i][0] == 'b') break;
    moves.push(id+i);
    if(pieces[id+i][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i] > -1; i++) {
    if(pieces[id-i][0] == 'b') break;
    moves.push(id-i);
    if(pieces[id-i][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]+i*16] > -1; i++) {
    if(pieces[id+i*14][0] == 'b') break;
    moves.push(id+i*14);
    if(pieces[id+i*14][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i*16] > -1; i++) {
    if(pieces[id-i*14][0] == 'b') break;
    moves.push(id-i*14);
    if(pieces[id-i*14][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]+i*17] > -1; i++) {
    if(pieces[id+i*15][0] == 'b') break;
    moves.push(id+i*15);
    if(pieces[id+i*15][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]+i*15] > -1; i++) {
    if(pieces[id+i*13][0] == 'b') break;
    moves.push(id+i*13);
    if(pieces[id+i*13][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i*17] > -1; i++) {
    if(pieces[id-i*15][0] == 'b') break;
    moves.push(id-i*15);
    if(pieces[id-i*15][0] == 'w') break;
  }
  for(i = 1; mb192[mb112[id]-i*15] > -1; i++) {
    if(pieces[id-i*13][0] == 'b') break;
    moves.push(id-i*13);
    if(pieces[id-i*13][0] == 'w') break;
  }
  return moves;
}

function white_king_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == BLACK_PICK_UP || state == BLACK_PUT_DOWN) return moves;
  if(mb192[mb112[id]+1] > -1 && pieces[id+1][0] != 'w') moves.push(id+1);
  if(mb192[mb112[id]-1] > -1 && pieces[id-1][0] != 'w') moves.push(id-1);
  if(mb192[mb112[id]+16] > -1 && pieces[id+14][0] != 'w') moves.push(id+14);
  if(mb192[mb112[id]-16] > -1 && pieces[id-14][0] != 'w') moves.push(id-14);
  if(mb192[mb112[id]+17] > -1 && pieces[id+15][0] != 'w') moves.push(id+15);
  if(mb192[mb112[id]-17] > -1 && pieces[id-15][0] != 'w') moves.push(id-15);
  if(mb192[mb112[id]+15] > -1 && pieces[id+13][0] != 'w') moves.push(id+13);
  if(mb192[mb112[id]-15] > -1 && pieces[id-13][0] != 'w') moves.push(id-13);
  // first king side castle
  if(id == 105 && pieces[106] == 'sp' && pieces[107] == 'sp' && pieces[108] == 'wr') {
    moves.push(107);
  }
  // second king side castle
  if(id == 107 && pieces[108] == 'sp' && pieces[109] == 'sp' && pieces[110] == 'sp' && pieces[111] == 'wr') {
    moves.push(109);
  }
  // first queen side castle
  if(id == 105 && pieces[104] == 'sp' && pieces[103] == 'sp' && pieces[102] == 'sp' && pieces[101] == 'wr') {
    moves.push(103);
  }
  // second queen side castle
  if(id == 103 && pieces[102] == 'sp' && pieces[101] == 'sp' && pieces[100] == 'sp' && pieces[99] == 'sp' && pieces[98] == 'wr') {
    moves.push(101);
  }
  return moves;
}

function black_king_moves(id, mb192, mb112, pieces) {
  moves = new Array();
//  if(state == WHITE_PICK_UP || state == WHITE_PUT_DOWN) return moves;
  if(mb192[mb112[id]+1] > -1 && pieces[id+1][0] != 'b') moves.push(id+1);
  if(mb192[mb112[id]-1] > -1 && pieces[id-1][0] != 'b') moves.push(id-1);
  if(mb192[mb112[id]+16] > -1 && pieces[id+14][0] != 'b') moves.push(id+14);
  if(mb192[mb112[id]-16] > -1 && pieces[id-14][0] != 'b') moves.push(id-14);
  if(mb192[mb112[id]+17] > -1 && pieces[id+15][0] != 'b') moves.push(id+15);
  if(mb192[mb112[id]-17] > -1 && pieces[id-15][0] != 'b') moves.push(id-15);
  if(mb192[mb112[id]+15] > -1 && pieces[id+13][0] != 'b') moves.push(id+13);
  if(mb192[mb112[id]-15] > -1 && pieces[id-13][0] != 'b') moves.push(id-13);
  // first king side castle
  if(id == 7 && pieces[8] == 'sp' && pieces[9] == 'sp' && pieces[10] == 'br') {
    moves.push(9);
  }
  // second king side castle
  if(id == 9 && pieces[10] == 'sp' && pieces[11] == 'sp' && pieces[12] == 'sp' && pieces[13] == 'br') {
    moves.push(11);
  }
  // first queen side castle
  if(id == 7 && pieces[6] == 'sp' && pieces[5] == 'sp' && pieces[4] == 'sp' && pieces[3] == 'br') {
    moves.push(5);
  }
  // second queen side castle
  if(id == 5 && pieces[4] == 'sp' && pieces[3] == 'sp' && pieces[2] == 'sp' && pieces[1] == 'sp' && pieces[0] == 'br') {
    moves.push(3);
  }
  return moves;
}

function white_pawn_moves(id, mb192, mb112, pieces, side, ep) {
  moves = new Array();
//  if(state == BLACK_PICK_UP || state == BLACK_PUT_DOWN) return moves;
  if(side == 'white') {
    // 2 rank move forward from initial position
    if(id >= 84 && id < 98 && pieces[id-14] == 'sp' && pieces[id-28] == 'sp') moves.push(id-28);
    // 1 rank move forward
    if(mb192[mb112[id]-16] > -1 && pieces[id-14] == 'sp') moves.push(id-14);
    // capture diagonal
    if(mb192[mb112[id]-17] > -1 && pieces[id-15][0] == 'b') moves.push(id-15);
    if(mb192[mb112[id]-15] > -1 && pieces[id-13][0] == 'b') moves.push(id-13);
    // en passant capture
    if(id == ep+1) moves.push(id-15);
    if(id == ep-1) moves.push(id-13);
  } else {
    // 2 rank move forward from initial position
    if(id >= 14 && id < 28 && pieces[id+14] == 'sp' && pieces[id+28] == 'sp') moves.push(id+28);
    // 1 rank move forward
    // 1 rank move forward
    if(mb192[mb112[id]+16] > -1 && pieces[id+14] == 'sp') moves.push(id+14);
    // capture diagonal
    if(mb192[mb112[id]+17] > -1 && pieces[id+15][0] == 'b') moves.push(id+15);
    if(mb192[mb112[id]+15] > -1 && pieces[id+13][0] == 'b') moves.push(id+13);
    // en passant capture
    if(id == ep-1) moves.push(id+15);
    if(id == ep+1) moves.push(id+13);
  }
  return moves;
}

function black_pawn_moves(id, mb192, mb112, pieces, side, ep) {
  moves = new Array();
//  if(state == WHITE_PICK_UP || state == WHITE_PUT_DOWN) return moves;
  if(side == 'white') {
    // 2 rank move forward from initial position
    if(id >= 14 && id < 28 && pieces[id+14] == 'sp' && pieces[id+28] == 'sp') moves.push(id+28);
    // 1 rank move forward
    // 1 rank move forward
    if(mb192[mb112[id]+16] > -1 && pieces[id+14] == 'sp') moves.push(id+14);
    // capture diagonal
    if(mb192[mb112[id]+17] > -1 && pieces[id+15][0] == 'w') moves.push(id+15);
    if(mb192[mb112[id]+15] > -1 && pieces[id+13][0] == 'w') moves.push(id+13);
    // en passant capture
    if(id == ep-1) moves.push(id+15);
    if(id == ep+1) moves.push(id+13);
  } else {
    // 2 rank move forward from initial position
    if(id >= 84 && id < 98 && pieces[id-14] == 'sp' && pieces[id-28] == 'sp') moves.push(id-28);
    // 1 rank move forward
    if(mb192[mb112[id]-16] > -1 && pieces[id-14] == 'sp') moves.push(id-14);
    // capture diagonal
    if(mb192[mb112[id]-17] > -1 && pieces[id-15][0] == 'w') moves.push(id-15);
    if(mb192[mb112[id]-15] > -1 && pieces[id-13][0] == 'w') moves.push(id-13);
    // en passant capture
    if(id == ep+1) moves.push(id-15);
    if(id == ep-1) moves.push(id-13);
  }
  return moves;
}

function legal_moves(id, side, ep) {
  pieces = init_pieces();
  piece = pieces[id];
  moves = new Array();
  mb192 = mailbox192();
  mb112 = mailbox112();
  switch(piece) {
    case 'wr': moves = white_rook_moves(id, mb192, mb112, pieces); break;
    case 'br': moves = black_rook_moves(id, mb192, mb112, pieces); break;
    case 'wn': moves = white_knight_moves(id, mb192, mb112, pieces); break;
    case 'bn': moves = black_knight_moves(id, mb192, mb112, pieces); break;
    case 'wb': moves = white_bishop_moves(id, mb192, mb112, pieces); break;
    case 'bb': moves = black_bishop_moves(id, mb192, mb112, pieces); break;
    case 'wq': moves = white_queen_moves(id, mb192, mb112, pieces); break;
    case 'bq': moves = black_queen_moves(id, mb192, mb112, pieces); break;
    case 'wk': moves = white_king_moves(id, mb192, mb112, pieces); break;
    case 'bk': moves = black_king_moves(id, mb192, mb112, pieces); break;
    case 'wp': moves = white_pawn_moves(id, mb192, mb112, pieces, side, ep); break;
    case 'bp': moves = black_pawn_moves(id, mb192, mb112, pieces, side, ep); break;
  }
  return moves;
}

